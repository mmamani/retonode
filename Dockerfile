FROM mhart/alpine-node
RUN mkdir /app
WORKDIR /app
COPY index.js  /app
COPY package.json /app
COPY node_modules /app
RUN apk add --no-cache  \
    && apk add --no-cache --virtual .build-deps .build-deps \
    && npm  install pg --save \
    && npm install \
    && apk del .build-deps
EXPOSE 4000
COPY . /app
CMD ["npm", "start"]
