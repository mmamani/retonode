const _express = require('express');
const _server = _express();

const _port = 4000;

_server.get('/retoibm/sumar/:sumando01/:sumando02', function(request, response) {
  try{
    var _sumando01 = new Number(request.params.sumando01);
    var _sumando02 = new Number(request.params.sumando02);
    var _resultado = _sumando01 + _sumando02;
    
    if (typeof _resultado !== "undefined" && _resultado!==null && !isNaN(_resultado)){    
      /* guardar en postgres
      var pg = require("pg")
      var connectionString = "pg://docker:docker@localhost:5432/sumas";
      var client = new pg.Client(connectionString);
      client.connect();
      module.exports = {
         async insertar(_sumando01, _sumando02, _resultado) {
        let resultados = await conexion.query(`insert into suma
        (_sumando01, _sumando02,_resultado)
        values
        ($1, $2)`, [nombre, precio]);
        return resultados;
      },
      }
      */
      return response.status(200).json({resultado : _resultado});
    }else{
      return response.status(400).json({resultado : "Bad Request"});
    }
  }
  catch(e){
    return response.status(500).json({resultado : e});
  }
});


_server.listen(_port, () => {
   console.log(`Server listening at ${_port}`);
});
